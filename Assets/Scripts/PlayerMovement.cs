﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {
    public Vector2 movement;
    public float speed = 1;
    Rigidbody2D rb;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        movement = Vector2.zero;
        if (Input.GetAxis("Horizontal") > 0.01f) {
            movement.x = 1;
        } else if (Input.GetAxis("Horizontal") < -0.01f) {
            movement.x = -1;
        }

        if (Input.GetAxis("Vertical") > 0.01f) {
            movement.y = 1;
        } else if (Input.GetAxis("Vertical") < -0.01f) {
            movement.y = -1;
        }
	}

    void FixedUpdate() {
        rb.velocity = movement * speed;
    }
}
